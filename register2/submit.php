<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$facultate = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$error = 0;
$sex = "";

if(isset($_POST['sex'])){
	$firstname = $_POST['sex'];
}

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['facultate'])){
	$birth = $_POST['facultate'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}

if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($facebook) || empty($birth) || empty($department) || empty($question) || empty($facultate) || empty($captcha_inserted) || empty($check)){
	$error = 1;
	$error_text = "One or more fields are empty!";
}

if(strlen($firstname) < 3 || strlen($firstname) > 20){
	$error = 1;
	$error_text = "First name is not valid!";
}

if(strlen($lastname) < 3 || strlen($lastname) > 20){
	$error = 1;
	$error_text = "Last name is not valid!";
}

if(strlen($facultate) < 3 || strlen($facultate) > 30){
$error = 1;
$error_text = "facultate is not valid!";
}

if(strlen($question) < 15){
	$error = 1;
	$error_text = "Question is shorter than expected!";
}

if (preg_match('/[^A-Za-z]/', $firstname) || preg_match('/[^A-Za-z]/', $lastname)){
    $error = 1;
	$error_text = "First name and Last name should only contain letters!";
}

if (preg_match('/[^A-Za-z]/', $facultate) || preg_match('/[^A-Za-z]/', $facultate)){
$error = 1;
$error_text = "facultate should only contain letters!";
}

if(!is_numeric($phone) || strlen($phone)!=10){
	$error = 1;
	$error_text = "Phone number is not valid!";
}

if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
	$error = 1;
	$error_text = "Email is not valid!";
}

if(strlen($cnp) !=13 || $cnp[0] != 1 && $cnp[0] != 2 && $cnp[0] != 3 && $cnp[0] != 4 && $cnp[0] != 5 && $cnp[0] != 6){
	$error = 1;
	$error_text = "CNP is not valid!";
}

if($cnp[0]==1 || $cnp[0]==3 || $cnp[0]==5){$sex= 'M';}	
else{
	if($cnp[0]==2 || $cnp[0]==4 || $cnp[0]==6){$sex= 'F';}	
	}

    $fbUrlCheck = '/^(https?:\/\/)?(www\.)?facebook.com\/[a-zA-Z0-9(\.\?)?]/';
	$secondCheck = '/home((\/)?\.[a-zA-Z0-9])?/';
	

	
if(preg_match($fbUrlCheck, $facebook) == 1 && preg_match($secondCheck, $facebook) == 0){}

		else {
		$error = 1;
	    $error_text = "Facebook link is not valid!";
	         }

 $date2=date("d-m-Y");//today's date

   $date1=new DateTime($birth);
   $date2=new DateTime($date2);

   $interval = $date1->diff($date2);

   $myage= $interval->y; 

  if ($myage < 18 || $myage >100)
  { 
     $error = 1;
	 $error_text = "You must be between 18 and 100 years old!";
  } 
  
if($captcha_inserted != $captcha_generated)
	{ 
     $error = 1;
	 $error_text = "Capcha code is incorrect!";
    }

if($birth[2] != $cnp[1] || $birth[3] != $cnp[2] || $birth[5] != $cnp[3] || $birth[6] != $cnp[4] || $birth[8] != $cnp[5] || $birth[9] != $cnp[6]){
	$error = 1;
	$error_text = "CNP and Birthdate are not compatible!"; // (X) Doubt
}

$conex=mysqli_connect('localhost','root','');

$selectdb=mysqli_select_db($conex,'ac');

if(mysqli_num_rows(mysqli_query($conex,"SELECT * FROM register2 WHERE email='$email'"))>0){
	$error=1;
	$error_text="Email has already been used!";
}

if(mysqli_num_rows(mysqli_query($conex,"SELECT * FROM register2 "))>49){
	$error=1;
	$error_text="The limit of 50 participants has been reached!";
}


try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}

if($error == 0) {

$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facebook,birth,department,question,sex) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facebook,:birth,:department,:question,:sex)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);
$stmt2 -> bindParam(':sex',$sex);

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

}else{
    echo "Succes!";}
}
else
        {
        echo $error_text;
        return;
        }



